import { Injectable } from '@angular/core';
import { Image } from './models/Image';

@Injectable()
export class ImagesService {
  imageStandardList: Image[] = [
    {
      url: '../../../assets/greyed/ask.png',
      alt: 'Ask',
      search: {
        search: true,
        images: false,
        videos: true
      }
    },
    {
      url: '../../../assets/greyed/dogpile.png',
      alt: 'Dogpile',
      search: {
        search: false,
        images: true,
        videos: false
      }
    },
    {
      url: '../../../assets/greyed/baidu.png',
      alt: 'Baidu',
      search: {
        search: true,
        images: true,
        videos: false
      }
    },
    {
      url: '../../../assets/greyed/bing.png',
      alt: 'Bing',
      search: {
        search: true,
        images: true,
        videos: true
      }
    },
    {
      url: '../../../assets/greyed/google.png',
      alt: 'Google',
      search: {
        search: true,
        images: true,
        videos: true
      }
    },
    {
      url: '../../../assets/greyed/youtube.png',
      alt: 'Youtube',
      search: {
        search: false,
        images: false,
        videos: true
      }
    },
    {
      url: '../../../assets/greyed/yahoo.png',
      alt: 'Yahoo',
      search: {
        search: true,
        images: true,
        videos: true
      }
    },
    {
      url: '../../../assets/greyed/yandex.png',
      alt: 'Yandex',
      search: {
        search: true,
        images: true,
        videos: true
      }
    },
    {
      url: '../../../assets/greyed/duckduckgo.png',
      alt: 'DuckDuckGo',
      search: {
        search: true,
        images: true,
        videos: true
      }
    }
  ]


  constructor() { }

  getImages() {
    return this.imageStandardList;
  }
}
