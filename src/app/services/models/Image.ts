import { Search } from './Search'

export interface Image {
    url: string;
    alt: string;
    search: Search;
}