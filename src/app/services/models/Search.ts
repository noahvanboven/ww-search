export interface Search {
    search?: boolean;
    images?: boolean;
    videos?: boolean;
}