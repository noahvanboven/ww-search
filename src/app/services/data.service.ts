import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  constructor() { }

  setInput(value): void {
    sessionStorage.setItem('inputvalue', value);
  }

  getInput() {
    return sessionStorage.getItem('inputvalue');
  }

}
