import { Component, OnInit } from '@angular/core';
import { ImagesService } from '../../services/images.service';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';

import { Image } from '../../services/models/Image';

@Component({
  selector: 'app-videosearch',
  templateUrl: './videosearch.component.html',
  styleUrls: ['./videosearch.component.css']
})
export class VideosearchComponent implements OnInit {
  value: string;
  images: Image[]
  searchurls: string[];

  constructor(public imagesService: ImagesService, public dataService: DataService, public router: Router) {
    router.events.subscribe(() => {
      (<HTMLInputElement>document.querySelector('.searchbar')).focus();
    })
  }

  ngOnInit() {
    this.value = this.dataService.getInput();
    this.images = this.imagesService.getImages().filter((image: Image) => {
      return image.search.videos === true;
    });
    this.searchurls = ['https://www.ask.com/youtube?q=', 'https://www.bing.com/videos/search?q=',
      'https://www.google.nl/search?tbm=vid&q=', 'https://www.youtube.com/results?search_query=',
      'https://video.search.yahoo.com/search/video?p=', 'https://yandex.com/video/search?text=',
      'https://duckduckgo.com/?iax=videos&q=']
  }

  handleSearch(event, searchurl: string) {
    window.location.href = searchurl + (<HTMLInputElement>document.querySelector('.searchbar')).value;
  }

  setSessionStorage(value: string) {
    this.dataService.setInput(value);
  }

  colorHover(event) {
    let src = event.target.src.replace('greyed', 'colored')
    event.target.src = src;
    event.target.addEventListener('mouseleave', () => {
      let src = event.target.src.replace('colored', 'greyed');
      event.target.src = src;
    })
  }
}
