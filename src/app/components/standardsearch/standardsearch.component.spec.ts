import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandardsearchComponent } from './standardsearch.component';

describe('StandardsearchComponent', () => {
  let component: StandardsearchComponent;
  let fixture: ComponentFixture<StandardsearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandardsearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
