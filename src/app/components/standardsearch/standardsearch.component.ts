import { Component, OnInit } from '@angular/core';
import { ImagesService } from '../../services/images.service';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';

import { Image } from '../../services/models/Image';

@Component({
  selector: 'app-standardsearch',
  templateUrl: './standardsearch.component.html',
  styleUrls: ['./standardsearch.component.css']
})
export class StandardsearchComponent implements OnInit {
  value: string;
  images: Image[]
  searchurls: string[];

  constructor(public imagesService: ImagesService, public dataService: DataService, public router: Router) {
    router.events.subscribe(() => {
      (<HTMLInputElement>document.querySelector('.searchbar')).focus();
    })
  }

  ngOnInit() {
    this.value = this.dataService.getInput();
    this.images = this.imagesService.getImages().filter((image: Image) => {
      return image.search.search === true;
    });
    this.searchurls = ['https://ask.com/web?q=', 'http://www.baidu.com/s?wd=', 'https://www.bing.com/search?q=', 'https://www.google.nl/search?q=',
      'https://search.yahoo.com/search?p=', 'https://yandex.com/search/?text=', 'https://duckduckgo.com/?q=']
  }

  handleSearch(event, searchurl: string) {
    window.location.href = searchurl + (<HTMLInputElement>document.querySelector('.searchbar')).value;
  }

  setSessionStorage(value: string) {
    this.dataService.setInput(value);
  }

  colorHover(event) {
    let src = event.target.src.replace('greyed', 'colored')
    event.target.src = src;
    event.target.addEventListener('mouseleave', () => {
      let src = event.target.src.replace('colored', 'greyed');
      event.target.src = src;
    })
  }
}