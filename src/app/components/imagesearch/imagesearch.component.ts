import { Component, OnInit } from '@angular/core';
import { ImagesService } from '../../services/images.service';
import { DataService } from '../../services/data.service';
import { Router } from '@angular/router';

import { Image } from '../../services/models/Image';

@Component({
  selector: 'app-imagesearch',
  templateUrl: './imagesearch.component.html',
  styleUrls: ['./imagesearch.component.css']
})
export class ImagesearchComponent implements OnInit {
  value: string;
  images: Image[];
  searchurls: string[];

  constructor(public imagesService: ImagesService, public dataService: DataService, public router: Router) {
    router.events.subscribe(() => {
      (<HTMLInputElement>document.querySelector('.searchbar')).focus();
    })
  }

  ngOnInit() {
    this.value = this.dataService.getInput();
    this.images = this.imagesService.getImages().filter((image: Image) => {
      return image.search.images === true;
    });
    this.searchurls = ['http://www.dogpile.com/search/images?q=', 'https://image.baidu.com/search/index?tn=baiduimage&word=', 'https://www.bing.com/images/search?q=',
      'https://www.google.nl/search?tbm=isch&q=', 'https://images.search.yahoo.com/search/images?p=', 'https://yandex.com/images/search?text=', 'https://duckduckgo.com/?ia=images&iax=images&q=']
  }

  handleSearch(event, searchurl: string) {
    window.location.href = searchurl + (<HTMLInputElement>document.querySelector('.searchbar')).value;
  }

  setSessionStorage(value: string) {
    this.dataService.setInput(value);
  }

  colorHover(event) {
    let src = event.target.src.replace('greyed', 'colored')
    event.target.src = src;
    event.target.addEventListener('mouseleave', () => {
      let src = event.target.src.replace('colored', 'greyed');
      event.target.src = src;
    })
  }
}
