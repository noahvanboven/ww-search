import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ImagesService } from './services/images.service';
import { DataService } from './services/data.service';

import { AppComponent } from './app.component';
import { StandardsearchComponent } from './components/standardsearch/standardsearch.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ImagesearchComponent } from './components/imagesearch/imagesearch.component';
import { VideosearchComponent } from './components/videosearch/videosearch.component';

const appRoutes: Routes = [
  {
    path: '',
    component: StandardsearchComponent
  },
  {
    path: 'image',
    component: ImagesearchComponent
  },
  {
    path: 'video',
    component: VideosearchComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    StandardsearchComponent,
    NavbarComponent,
    ImagesearchComponent,
    VideosearchComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [ImagesService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
